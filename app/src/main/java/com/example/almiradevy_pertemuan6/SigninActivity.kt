package com.example.almiradevy_pertemuan6

import android.os.Bundle
import android.os.PersistableBundle
import androidx.annotation.ContentView
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signin.*

class SigninActivity : AppCompatActivity() {

    var fbAuth = FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        btnLogoff.setOnClickListener{
            fbAuth.signOut()
        }

    }

}